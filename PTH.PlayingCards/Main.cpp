
// Playing Cards
// Paul Haller
// Jason Clemons
//Ricardo Guzman Ortiz

#include <iostream>
#include <conio.h>

using namespace std;

enum class Suit
{
	SPADES,
	DIAMONDS,
	HEARTS,
	CLUBS
};

enum class Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main()
{
	Card c1;
	c1.Rank = Rank::QUEEN;
	c1.Suit = Suit::DIAMONDS;

	Card c2;
	c2.Rank = Rank::ACE;
	c2.Suit = Suit::HEARTS;
	PrintCard(HighCard(c1,c2));
	

	(void)_getch();
	return 0;
}


void PrintCard(Card card)
{
	//find rank
	string rank = "";
	switch (card.Rank)
	{
	case Rank::TWO: rank = "2 Of ";
		break;
	case Rank::THREE: rank = "3 Of ";
		break;
	case Rank::FOUR: rank = "4 Of ";
		break;
	case Rank::FIVE: rank = "5 Of ";
		break;
	case Rank::SIX: rank = "6 Of ";
		break;
	case Rank::SEVEN: rank = "7 Of ";
		break;
	case Rank::EIGHT: rank = "8 Of ";
		break;
	case Rank::NINE: rank = "9 Of ";
		break;
	case Rank::TEN: rank = "10 Of ";
		break;
	case Rank::JACK: rank = "Jack Of ";
		break;
	case Rank::QUEEN: rank = "Queen Of ";
		break;
	case Rank::KING: rank = "King Of ";
		break;
	case Rank::ACE: rank = "Ace Of ";
		break;

	default:
		break;

		
	}


	// find suit
	string suit = "";
	switch (card.Suit)
	{
	case Suit::CLUBS: suit= "Clubs";
		break;
	case Suit::DIAMONDS: suit = "Dimonds";
		break;
	case Suit::HEARTS: suit = "Heats";
		break;
	case Suit::SPADES: suit = "Spades";
		break;
	default:
		break;
	}
	cout<< rank << suit;
}
Card HighCard(Card card1, Card card2)
{
	
	if (card1.Rank > card2.Rank)
		return card1;
	else
		return card2;
}
